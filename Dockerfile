FROM rust:1.46 as builder
WORKDIR /usr/src/kicad-redirect
COPY . .
RUN cargo install --path .



FROM debian:buster-slim
EXPOSE 3000
ENV TZ=Etc/UTC \
    APP_USER=1000

RUN apt-get update \
    && apt-get install -y ca-certificates tzdata libc6 \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /app
WORKDIR /app
COPY --from=builder /usr/local/cargo/bin/kicad-redirect /app/kicad-redirect
COPY ./config /app/config

RUN chown -R $APP_USER:$APP_USER kicad-redirect

USER $APP_USER

CMD ["./kicad-redirect"]